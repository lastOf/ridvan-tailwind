export default function Card({data}) {
    const {title, subtitle, paragraph, image} = data;
  return (
    <div className="xl:w-1/4 md:w-1/2 p-4">
      <div className="bg-gray-100 p-6 rounded-lg">
        <img
          className="h-40 rounded w-full object-cover object-center mb-6"
          src={image}
          alt={subtitle}
        />
        <h3 className="tracking-widest text-teal-700 text-xs font-medium title-font">
          {subtitle}
        </h3>
        <h2 className="text-lg text-gray-900 font-medium title-font mb-4">
          {title}
        </h2>
        <p className="leading-relaxed text-base">
          {paragraph}{" "}
        </p>
          {/* <a className="mt-3 text-teal-700 inline-flex items-center">
            Learn More
            <svg
              fill="none"
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              className="w-4 h-4 ml-2"
              viewBox="0 0 24 24"
            >
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </a> */}
      </div>
    </div>
  );
}
