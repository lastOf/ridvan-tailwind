import { useEffect, useState } from "react";

export default function smallBizForm() {
  return (
    <section>
      <div className="container mx-auto flex px-5 py-5 md:flex-row flex-col-reverse items-center">
        <form
          id="bizform"
          action="https://usebasin.com/f/87e0613e08c2"
          method="POST"
        >
          <div>
            <div>
              <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                  <div className="px-4 sm:px-0">
                    <h3 className="text-lg font-medium leading-6 text-gray-900">
                      Business Profile
                    </h3>
                    <p className="mt-1 text-sm text-gray-600">
                      This information will help customers to understand and
                      connect with your business. Fill this information out as
                      you would have it displayed on your website, for example
                      with the proper casing.
                    </p>
                  </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                  <div className="shadow overflow-hidden sm:rounded-md">
                    <div className="px-4 py-5 bg-white sm:p-6">
                      <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="business_name"
                            className="block text-sm font-medium text-gray-700"
                          >
                            What is the <b>name of your business</b>?
                          </label>
                          <input
                            type="text"
                            name="business_name"
                            id="business_name"
                            autoComplete="Organization"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="tag_line"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Give us a <b>tag line</b>. To help customers quickly
                            recognize what is most important to you.
                          </label>
                          <p className="mt-1 text-sm text-gray-500">
                            Something like "Trampolines you can trust!" or
                            "We'll get you home in one piece."
                          </p>
                          <input
                            type="text"
                            name="tag_line"
                            id="tag_line"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                      <div className="mt-6">
                        <label
                          htmlFor="about"
                          className="block text-sm font-medium text-gray-700"
                        >
                          <b>Describe</b> your business to your customers.
                        </label>
                        <p className="mt-1 text-sm text-gray-500">
                          This will be the main paragraph on your page.
                        </p>
                        <div className="mt-1">
                          <textarea
                            id="about"
                            name="about"
                            rows={3}
                            className="shadow-sm focus:ring-teal-500 focus:border-teal-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md"
                            placeholder="Ridvan.org is a local software consulting agency. We try to put useful and modern software in the hands who often don't have the resources to find it themselves."
                            defaultValue={""}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a
                        href="#part2"
                        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500"
                      >
                        Now add business information below
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden sm:block" aria-hidden="true">
              <div className="py-5">
                <div className="border-t border-gray-200" />
              </div>
            </div>
            <div id="part2" className="mt-10 sm:mt-0">
              <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                  <div className="px-4 sm:px-0">
                    <h3 className="text-lg font-medium leading-6 text-gray-900">
                      Business Information
                    </h3>
                    <p className="mt-1 text-sm text-gray-600">
                      Let customers know when, where, and how they can reach
                      you. If a field doesn't apply to your business just leave
                      it blank.
                    </p>
                  </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                  <div className="shadow overflow-hidden sm:rounded-md">
                    <div className="px-4 py-5 bg-white sm:p-6">
                      <div className="grid grid-cols-6 gap-6">
                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="hours"
                            className="block text-sm font-medium text-gray-700"
                          >
                            In a sentence or two{" "}
                            <b>describe your business hours</b>.
                          </label>
                          <p className="mt-1 text-sm text-gray-500">
                            "We're open weekdays from 9am-5pm, and Saturdays
                            from 9am-3pm."
                          </p>
                          <input
                            type="text"
                            name="hours"
                            id="hours"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="email_address"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Email address
                          </label>
                          <input
                            type="text"
                            name="email_address"
                            id="email_address"
                            autoComplete="email"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="phone"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Phone Number
                          </label>
                          <input
                            type="tel"
                            name="phone"
                            id="phone"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-3">
                          <label
                            htmlFor="country"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Country / Region
                          </label>
                          <select
                            id="country"
                            name="country"
                            autoComplete="country"
                            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-teal-500 focus:border-teal-500 sm:text-sm"
                          >
                            <option>United States</option>
                            <option>Canada</option>
                            <option>Mexico</option>
                          </select>
                        </div>
                        <div className="col-span-6">
                          <label
                            htmlFor="street_address"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Street address
                          </label>
                          <input
                            type="text"
                            name="street_address"
                            id="street_address"
                            autoComplete="street-address"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                          <label
                            htmlFor="city"
                            className="block text-sm font-medium text-gray-700"
                          >
                            City
                          </label>
                          <input
                            type="text"
                            name="city"
                            id="city"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                          <label
                            htmlFor="state"
                            className="block text-sm font-medium text-gray-700"
                          >
                            State / Province
                          </label>
                          <input
                            type="text"
                            name="state"
                            id="state"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                        <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                          <label
                            htmlFor="postal_code"
                            className="block text-sm font-medium text-gray-700"
                          >
                            ZIP / Postal
                          </label>
                          <input
                            type="text"
                            name="postal_code"
                            id="postal_code"
                            autoComplete="postal-code"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                      <fieldset className="mt-6">
                        <div>
                          <legend className="text-base font-medium text-gray-900">
                            Best case scenario a customer...
                          </legend>
                          <p className="text-sm text-gray-500">
                            (What is your preferred way of being contacted about
                            business?)
                          </p>
                        </div>
                        <div className="mt-4 space-y-4">
                          <div className="flex items-center">
                            <input
                              id="email"
                              name="preferred_contact"
                              type="radio"
                              value="email"
                              className="focus:ring-teal-500 h-4 w-4 text-teal-600 border-gray-300"
                            />
                            <label
                              htmlFor="email"
                              className="ml-3 block text-sm font-medium text-gray-700"
                            >
                              Emails you.
                            </label>
                          </div>
                          <div className="flex items-center">
                            <input
                              id="phone"
                              name="preferred_contact"
                              type="radio"
                              value="phone"
                              className="focus:ring-teal-500 h-4 w-4 text-teal-600 border-gray-300"
                            />
                            <label
                              htmlFor="phone"
                              className="ml-3 block text-sm font-medium text-gray-700"
                            >
                              Calls the business by phone.
                            </label>
                          </div>
                          <div className="flex items-center">
                            <input
                              id="arrives"
                              name="preferred_contact"
                              type="radio"
                              value="arrives"
                              className="focus:ring-teal-500 h-4 w-4 text-teal-600 border-gray-300"
                            />
                            <label
                              htmlFor="arrives"
                              className="ml-3 block text-sm font-medium text-gray-700"
                            >
                              Shows up at your business location.
                            </label>
                          </div>
                          <div className="flex items-center">
                            <input
                              id="mail"
                              name="preferred_contact"
                              type="radio"
                              value="mail"
                              className="focus:ring-teal-500 h-4 w-4 text-teal-600 border-gray-300"
                            />
                            <label
                              htmlFor="mail"
                              className="ml-3 block text-sm font-medium text-gray-700"
                            >
                              Sends mail to your business address.
                            </label>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a
                        href="#part3"
                        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500"
                      >
                        Next list 4 services
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden sm:block" aria-hidden="true">
              <div className="py-5">
                <div className="border-t border-gray-200" />
              </div>
            </div>
            <div id="part3" className="mt-10 sm:mt-0">
              <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                  <div className="px-4 sm:px-0">
                    <h3 className="text-lg font-medium leading-6 text-gray-900">
                      Business Services, Advantages or Benefits
                    </h3>
                    <p className="mt-1 text-sm text-gray-600">
                      These four points describe the primary services you offer,
                      or the main benefits of working with you.
                    </p>
                  </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                  <div className="shadow overflow-hidden sm:rounded-md">
                    <div className="px-4 py-5 bg-white sm:p-6">
                      <div className="grid grid-cols-6 gap-2">
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="first_title"
                            className="block text-sm font-medium text-gray-700"
                          >
                            What is the{" "}
                            <b>
                              title of the most important service or benefit
                            </b>{" "}
                            clients receive when they choose you?
                          </label>
                          <p className="mt-1 text-sm text-gray-500">
                            Such as "A quote you can trust."
                          </p>
                          <input
                            type="text"
                            name="first_title"
                            id="first_title"
                            autoComplete=""
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="first_description"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Now <b>describe that first point</b>.
                          </label>
                          <p className="mt-1 text-sm text-gray-500">
                            For example, "There's no doubt that truthfulness is
                            the most important way to start any business
                            dealing."
                          </p>
                          <input
                            type="text"
                            name="first_description"
                            id="first_description"
                            autoComplete="given-name"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                      <div className="grid grid-cols-6 gap-2 mt-8">
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="second_title"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Second <b>service or benefit title</b>?
                          </label>
                          <input
                            type="text"
                            name="second_title"
                            id="second_title"
                            autoComplete=""
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="second_description"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Describe the <b>second</b> point.
                          </label>
                          <input
                            type="text"
                            name="second_description"
                            id="second_description"
                            autoComplete="given-name"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                      <div className="grid grid-cols-6 gap-2 mt-8">
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="third_title"
                            className="block text-sm font-medium text-gray-700"
                          >
                            The third <b>service or benefit title</b>?
                          </label>
                          <input
                            type="text"
                            name="third_title"
                            id="third_title"
                            autoComplete=""
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="third-description"
                            className="block text-sm font-medium text-gray-700"
                          >
                            <b>Third</b> point description:
                          </label>
                          <input
                            type="text"
                            name="third_description"
                            id="third_description"
                            autoComplete="given-name"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                      <div className="grid grid-cols-6 gap-2 mt-8">
                        <div className="col-span-6 sm:col-span-4">
                          <label
                            htmlFor="fourth_title"
                            className="block text-sm font-medium text-gray-700"
                          >
                            The last listed <b>service or benefit title</b>?
                          </label>
                          <input
                            type="text"
                            name="fourth_title"
                            id="fourth_title"
                            autoComplete=""
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>

                        <div className="col-span-6 sm:col-span-6">
                          <label
                            htmlFor="fourth_description"
                            className="block text-sm font-medium text-gray-700"
                          >
                            Describe the <b>last</b> point.
                          </label>
                          <input
                            type="text"
                            name="fourth_description"
                            id="fourth_description"
                            autoComplete="given-name"
                            className="mt-1 focus:ring-teal-500 focus:border-teal-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a href="#">
                        <button
                          type="submit"
                          className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500"
                        >
                          Submit
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden sm:block" aria-hidden="true">
              <div className="py-5">
                <div className="border-t border-gray-200" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  );
}
