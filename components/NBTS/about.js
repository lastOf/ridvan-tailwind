export default function AboutNBTS({ content }) {
  return (
    <section className="text-gray-600 body-font overflow-hidden">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20 flex-col items-center text-center">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
            Frequently Asked Questions
          </h1>
        </div>
        <div className="-my-8 divide-y-2 divide-gray-100">
          {content.map((el) => {
            return (
              <>
                <div
                  key={el.question}
                  className="py-8 flex flex-wrap md:flex-nowrap"
                >
                  <div className="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
                    <span className="font-bold title-font text-gray-700">
                      {el.question}
                    </span>
                    <span className="mt-1 text-gray-500 text-sm">
                      {el.belowQuestion}
                    </span>
                  </div>
                  <div className="md:flex-grow">
                    <h2 className="text-2xl font-medium text-gray-900 title-font mb-2">
                      {el.heading}
                    </h2>
                    <p className="leading-relaxed">{el.body}</p>

                    <a
                      className="text-green-500 inline-flex items-center mt-4"
                      href={el.href}
                      onClick={el.click}
                    >
                      {el.linkText}
                      <svg
                        className="w-4 h-4 ml-2"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <path d="M5 12h14" />
                        <path d="M12 5l7 7-7 7" />
                      </svg>
                    </a>
                  </div>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </section>
  );
}
