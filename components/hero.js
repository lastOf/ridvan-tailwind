export default function Hero() {
  return (
    <section className="text-gray-700 body-font">
      <div className="container mx-auto flex px-5 py-5 md:flex-row flex-col-reverse items-center">
        <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
          <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
            Connecting you with <br className="hidden lg:inline-block" />
            Powerful Software
          </h1>
          <p className="mb-8 leading-relaxed">
            <b>Ridván</b> is here to help you navigate the tremendous sea of
            technology available today! Reach out to us at <b>lastof@pm.me</b>{" "}
            for a consultation.
          </p>
          <div className="flex justify-center">
            <a href="#opensource">
              <button className="inline-flex text-white bg-teal-700 border-0 py-2 px-6 focus:outline-none hover:bg-teal-600 rounded text-lg">
                Open Source Software
              </button>
            </a>
            <a href="#websites">
              <button className="ml-4 inline-flex text-gray-700 bg-gray-200 border-0 py-2 px-6 focus:outline-none hover:bg-gray-300 rounded text-lg">
                I'm looking for a website
              </button>
            </a>
          </div>
        </div>
        <div className="lg:max-w-lg py-5 lg:w-full md:w-1/2 w-5/6">
          <img
            className="object-cover object-center rounded"
            alt="Beautiful flowers in a field."
            src={`/flower${Math.ceil(Math.random() * 4)}.jpg`}
          />
        </div>
      </div>
    </section>
  );
}
