import Head from "next/head";

export default function PageHead({
  title = "Ridván: software consulting agency",
  image = "/ridvanCard.jpg",
  description = "Ridván: a software consulting agency by Quddús George located Waterloo SC near Greenwood. Websites, Open Source Software, Mobile apps, Custom Software",
  shortDescription = "Software solutions from open source to Websites and Custom Apps.",
}) {
  return (
    <Head>
      <meta charSet="utf-8"></meta>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, viewport-fit=cover"
      ></meta>
      <title>{title}</title>
      <link rel="icon" href="/rose.svg" />
      <meta name="description" content={description}></meta>
      <meta name="robots" content="index, follow" />
      <link rel="canonical" href="https://ridvan.org/" />
      <link rel="apple-touch-icon" href="/rose.svg"></link>
      <meta name="apple-mobile-web-app-capable" content="yes"></meta>
      <html dir="ltr" lang="en"></html>
      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://ridvan.org" />
      <meta property="og:title" content={title} />
      <meta property="og:image" content={image} />
      <meta property="og:description" content={shortDescription} />
      <meta property="og:site_name" content="Ridván" />
      <meta property="og:locale" content="en_US" />
      {/* <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630"></meta> */}

      <meta name="twitter:card" content={shortDescription} />
      {/* <meta name="twitter:site" content="@site_account" />
      <meta name="twitter:creator" content="@individual_account" /> */}
      <meta name="twitter:url" content="https://www.ridvan.org" />
      <meta name="twitter:title" content="Ridván" />
      <meta
        name="twitter:description"
        content="Ridván software consulting, helping you navigate todays software landscape."
      />
      <meta name="twitter:image" content={image}></meta>
      <script async defer data-domain="version1.ridvan.org" src="https://stats.ridvan.org/js/plausible.js"></script>

    </Head>
  );
}
