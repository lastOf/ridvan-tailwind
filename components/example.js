export default function Example({data}) {
    const {title, subtitle, image, avatar, paragraph, link } = data;
  return (
    <section className="text-gray-700 body-font">
      <div className="container px-5 pb-24 mx-auto flex flex-col">
        <div className="lg:w-4/6 mx-auto">
          <div className="rounded-lg h-72 overflow-hidden">
            <img
              alt={title}
              className="object-cover object-center h-full w-full"
              src={image}
            />
          </div>
          <div className="flex flex-col sm:flex-row mt-10">
            <div className="sm:w-1/3 text-center sm:pr-8 sm:py-8">
              <div className="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
              <img
              alt={subtitle}
              className="object-cover object-center"
              src={avatar}
              style={{borderRadius: 50}}
            />
              </div>
              <div className="flex flex-col items-center text-center justify-center">
                <h2 className="font-medium title-font mt-4 text-gray-900 text-lg">
                  {title}
                </h2>
                <div className="w-12 h-1 bg-teal-700 rounded mt-2 mb-4"></div>
                <p className="text-base text-gray-700">
                 {subtitle}
                </p>
              </div>
            </div>
            <div className="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-gray-300 sm:border-t-0 border-t mt-4 pt-4 sm:mt-0 text-center sm:text-left">
              <p className="leading-relaxed text-lg mb-4">
                {paragraph}
              </p>
              <a rel="author" className="text-teal-700 inline-flex items-center" href={link}>
                Check it Out
                <svg
                  fill="none"
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  className="w-4 h-4 ml-2"
                  viewBox="0 0 24 24"
                >
                  <path d="M5 12h14M12 5l7 7-7 7"></path>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
