import { useEffect, useState } from "react";
import Nav2 from "../components/nav2";
import Hero from "../components/hero";
import PageHead from "../components/head";
import SmallBizForm from "../components/NBTS/smallBizForm";
import About from "../components/NBTS/about";
import Footer from "../components/footer";

import Websites from "../components/websites";
import Example from "../components/example";

export default function Small() {
  let [view, setView] = useState("welcome");
  useEffect(() => {
    let form = document.getElementById("bizform");
    function handleForm(event) {
      event.preventDefault();

      fetch("https://usebasin.com/f/87e0613e08c2", {
        method: "post",
        body: new FormData(document.querySelector("form")),
      }).then((response) => {
        console.log(response);
        setView("complete");
      });
    }
    form.addEventListener("submit", handleForm);
  }, []);

  let aboutArr = [
    {
      question: "What do I need?",
      belowQuestion: "Getting Started",
      heading: "Your business information, 5 images, and $50.",
      body:
        "Be ready to list your business' name, a tagline or motto, a paragraph describing the business, a phone number, email address, and location (only include those if you want them included in the site). Four advantages of choosing your business, these could be products, services, qualities etc. Also prepare five images that represent your business. After you've sent us all of that, a one time payment of $50 and we will have your site live within 48 hours. ",
      linkText: "Lets do this",
      click: () => setView("form"),
      href: "#",
    },
    {
      question: "How is it so cheap?",
      belowQuestion: "Pricing",
      heading: "Simple, opinionated, and optimized",
      body:
        "These websites only provide your basic business information. You won't be able to upload posts, handle orders, chat with customers, or add your own customizations. You also will not be able to choose your own domain name (eg. Ridvan.org), we will choose a subdomain for you. By sacrificing those features, and bringing everything you want listed on the site up front to the application form we can cut out the major costs, including time, and get you up on the internet looking professional.",
      linkText: "Actually, I'm looking for some of those features",
      click: null,
      href: "https://ridvan.org/#websites",
    },
    {
      question: "Where do I upload images?",
      belowQuestion: "Completing your application",
      heading: "Just need to upload your images?",
      body:
        "If you were unable to upload your images when you completed your form you can add them here. Please edit the file names to match your businessname, for example: ridvan1.jpg, ridvan2.jpg. This way we know who is who.",
      linkText: "Upload your business images",
      click: () => setView("complete"),
      href: "#",
    },
  ];

  let pageHeadContent = {
    title: "No Business Too Small | Ridván",
    image: "/nbts.jpg",
    description:
      "Professional Websites for the limited budget. Beautiful, performant and accessible $50 websites to get your business an excellent web presence.",
    shortDescription:
      "Everything you need for a professional web presence, without having to bet the farm for it.",
  };

  return (
    <div>
      <PageHead
        title={pageHeadContent.title}
        image={pageHeadContent.image}
        description={pageHeadContent.description}
        shortDescription={pageHeadContent.shortDescription}
      />
      <Nav2></Nav2>
      <div
        style={{
          maxHeight: view.includes("welcome") ? 1800 : 0,
          overflow: "hidden",
          transition: "maxHeight, 2s",
          transitionTimingFunction: "ease-out",
        }}
      >
        <section className="text-gray-400 bg-gray-900 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="text-center mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium text-center title-font text-white mb-4">
                $50 Websites with the <i>No Business Too Small Program</i>
              </h1>
              <p className="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto">
                Everything you need to get started with a professional web
                presence, without having to bet the farm for it.
              </p>
            </div>
            <div className="flex flex-wrap lg:w-4/5 sm:mx-auto sm:mb-2 -mx-2">
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Beautiful on phones and computers
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Shows up in search results
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Professional Design
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Basic business information
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Completely Ad free
                  </span>
                </div>
              </div>{" "}
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={3}
                    className="text-teal-400 w-6 h-6 flex-shrink-0 mr-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                    <path d="M22 4L12 14.01l-3-3" />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Performant and Accessible
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    className="text-red-400 w-6 h-6 flex-shrink-0 mr-4"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Custom layout and branding
                  </span>
                </div>
              </div>
              <div className="p-2 sm:w-1/2 w-full">
                <div className="bg-gray-800 rounded flex p-4 h-full items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    className="text-red-400 w-6 h-6 flex-shrink-0 mr-4"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                  <span className="title-font font-medium text-white">
                    Custom domain name
                  </span>
                </div>
              </div>
            </div>

            <div className="flex justify-center mt-6">
              <a href="#">
                <button
                  onClick={() => setView("form")}
                  className="inline-flex text-white bg-teal-500 border-0 py-2 px-6 focus:outline-none hover:bg-teal-600 rounded text-lg"
                >
                  Apply
                </button>
              </a>
            </div>
          </div>
        </section>
        <section className="text-gray-400 bg-gray-900 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="xl:w-1/2 lg:w-3/4 w-full mx-auto text-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                className="inline-block w-8 h-8 text-gray-500 mb-8"
                viewBox="0 0 975.036 975.036"
              >
                <path d="M925.036 57.197h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.399 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l36 76c11.6 24.399 40.3 35.1 65.1 24.399 66.2-28.6 122.101-64.8 167.7-108.8 55.601-53.7 93.7-114.3 114.3-181.9 20.601-67.6 30.9-159.8 30.9-276.8v-239c0-27.599-22.401-50-50-50zM106.036 913.497c65.4-28.5 121-64.699 166.9-108.6 56.1-53.7 94.4-114.1 115-181.2 20.6-67.1 30.899-159.6 30.899-277.5v-239c0-27.6-22.399-50-50-50h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.4 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l35.9 75.8c11.601 24.399 40.501 35.2 65.301 24.399z" />
              </svg>
              <p className="leading-relaxed text-lg">
                We believe that the internet should be accessible to everyone.
                This <i>No Business Too Small</i> program is an effort along
                those lines. We can get you started with the basics, while still
                maintaining our high standards of quality and design.
              </p>
              <span className="inline-block h-1 w-10 rounded bg-teal-500 mt-8 mb-6" />
              <h2 className="text-white font-medium title-font tracking-wider text-sm">
                Quddús George
              </h2>
              <p className="text-gray-500">Ridván Software Consulting</p>
            </div>
            <button
              onClick={() => {
                view.includes("learn")
                  ? setView("welcome")
                  : setView("welcome learn");
              }}
              className="flex mx-auto mt-6 text-gray-700 bg-gray-100 border-0 py-2 px-6 focus:outline-none hover:bg-gray-200 rounded text-lg"
            >
              {view.includes("learn") ? "Show Less" : "Learn More"}
            </button>
          </div>
        </section>
      </div>
      <div
        style={{
          maxHeight: view.includes("learn") ? 3800 : 0,
          overflow: "hidden",
          transition: "maxHeight, 2s",
          transitionTimingFunction: "ease-out",
        }}
      >
        <div>
          <section className="text-gray-600 body-font">
            <div className="container px-5 py-24 mx-auto flex flex-wrap">
              <div className="flex flex-wrap w-full mb-20 flex-col items-center text-center">
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
                  How it works
                </h1>
              </div>
              <div className="flex flex-wrap w-full">
                <div className="lg:w-2/5 md:w-1/2 xl:w-1/2 md:pr-10 md:py-6">
                  <div className="flex relative pb-12">
                    <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                      <div className="h-full w-1 bg-gray-200 pointer-events-none" />
                    </div>
                    <div className="flex-shrink-0 w-10 h-10 rounded-full bg-teal-500 inline-flex items-center justify-center text-white relative z-10">
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        className="w-5 h-5"
                        viewBox="0 0 24 24"
                      >
                        <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z" />
                      </svg>
                    </div>
                    <div className="flex-grow pl-4">
                      <h2 className="font-medium title-font text-sm text-gray-900 mb-1 tracking-wider">
                        STEP 1
                      </h2>
                      <p className="leading-relaxed">
                        Apply for a small business website by completing the
                        above form.
                      </p>
                    </div>
                  </div>
                  <div className="flex relative pb-12">
                    <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                      <div className="h-full w-1 bg-gray-200 pointer-events-none" />
                    </div>
                    <div className="flex-shrink-0 w-10 h-10 rounded-full bg-teal-500 inline-flex items-center justify-center text-white relative z-10">
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        className="w-5 h-5"
                        viewBox="0 0 24 24"
                      >
                        <path d="M22 12h-4l-3 9L9 3l-3 9H2" />
                      </svg>
                    </div>
                    <div className="flex-grow pl-4">
                      <h2 className="font-medium title-font text-sm text-gray-900 mb-1 tracking-wider">
                        STEP 2
                      </h2>
                      <p className="leading-relaxed">
                        Upload 5 pictures that represent your business, and
                        we'll choose the ones that fit best.
                      </p>
                    </div>
                  </div>
                  <div className="flex relative pb-12">
                    <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                      <div className="h-full w-1 bg-gray-200 pointer-events-none" />
                    </div>
                    <div className="flex-shrink-0 w-10 h-10 rounded-full bg-teal-500 inline-flex items-center justify-center text-white relative z-10">
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        className="w-5 h-5"
                        viewBox="0 0 24 24"
                      >
                        <circle cx={12} cy={5} r={3} />
                        <path d="M12 22V8M5 12H2a10 10 0 0020 0h-3" />
                      </svg>
                    </div>
                    <div className="flex-grow pl-4">
                      <h2 className="font-medium title-font text-sm text-gray-900 mb-1 tracking-wider">
                        STEP 3
                      </h2>
                      <p className="leading-relaxed">
                        If everything looks good, we'll send you an email
                        confirming that you qualify for the small business
                        program.
                      </p>
                    </div>
                  </div>
                  <div className="flex relative pb-12">
                    <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                      <div className="h-full w-1 bg-gray-200 pointer-events-none" />
                    </div>
                    <div className="flex-shrink-0 w-10 h-10 rounded-full bg-teal-500 inline-flex items-center justify-center text-white relative z-10">
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        className="w-5 h-5"
                        viewBox="0 0 24 24"
                      >
                        <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2" />
                        <circle cx={12} cy={7} r={4} />
                      </svg>
                    </div>
                    <div className="flex-grow pl-4">
                      <h2 className="font-medium title-font text-sm text-gray-900 mb-1 tracking-wider">
                        STEP 4
                      </h2>
                      <p className="leading-relaxed">
                        The confirmation email will contain an invoice link for
                        a one-time fee of $50.
                      </p>
                    </div>
                  </div>
                  <div className="flex relative">
                    <div className="flex-shrink-0 w-10 h-10 rounded-full bg-teal-500 inline-flex items-center justify-center text-white relative z-10">
                      <svg
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        className="w-5 h-5"
                        viewBox="0 0 24 24"
                      >
                        <path d="M22 11.08V12a10 10 0 11-5.93-9.14" />
                        <path d="M22 4L12 14.01l-3-3" />
                      </svg>
                    </div>
                    <div className="flex-grow pl-4">
                      <h2 className="font-medium title-font text-sm text-gray-900 mb-1 tracking-wider">
                        Complete
                      </h2>
                      <p className="leading-relaxed">
                        We'll have your new website up and running within 48
                        hours.
                      </p>
                    </div>
                  </div>
                  {/* <a href="#">
                    <button
                      onClick={() => {
                        setView("form");
                      }}
                      className="flex mx-auto mt-6 text-gray-200 bg-teal-600 border-0 py-2 px-6 focus:outline-none hover:bg-teal-200 rounded text-lg"
                    >
                      Apply
                    </button>
                  </a> */}
                </div>
                <div className="w-full lg:w-3/5 md:w-1/2 xl:w-1/2 flex flex-col justify-center  ">
                  <img
                    className=" object-cover object-center md:mt-0 mt-12 rounded-lg  "
                    src="/nbts.jpg"
                    alt="No business too small banner"
                  />
                </div>
              </div>
            </div>
          </section>

          <About content={aboutArr} />
        </div>
      </div>

      <div
        style={{
          maxHeight: view.includes("form") ? 3800 : 0,
          overflow: "hidden",
          transition: "maxHeight, 1s",
          transitionTimingFunction: "ease-out",
        }}
      >
        <div className="grid grid-cols-4 bg-gray-900 mt-6">
          <div className="col-span-1">
            <button
              onClick={() => {
                setView("welcome");
              }}
              className=" ml-4 my-4 inline-flex text-gray-700 bg-gray-100 border-0 py-1 px-2 focus:outline-none hover:bg-gray-200 rounded text-sm"
            >
              Back
            </button>
          </div>
          <h1 className="col-span-4 sm:text-3xl text-2xl font-medium text-center title-font text-gray-100 mb-4">
            $50 Website Application Form
          </h1>
        </div>
        <SmallBizForm />
      </div>
      <section
        style={{
          maxHeight: view.includes("complete") ? 1000 : 0,
          overflow: "hidden",
          transition: "maxHeight, 1s",
          transitionTimingFunction: "ease-out",
        }}
        className="text-gray-400 bg-gray-900 body-font"
      >
        <div className="container px-5 py-24 mx-auto">
          <div className="lg:w-2/3 flex flex-col sm:flex-row sm:items-center items-start mx-auto">
            <div>
              <h1 className="flex-grow sm:pr-16 text-2xl font-medium title-font text-white">
                Last thing! Upload 5 images that represent your business.
              </h1>
              <p>
                We'll send you a link to pay and have your website up within 48
                hours of your payment.
              </p>
            </div>
            <a
              href="https://www.dropbox.com/request/HgdVISrkwgL8abKuItcY"
              target="_blank"
              className="flex-shrink-0 text-white bg-green-500 border-0 py-2 px-8 focus:outline-none hover:bg-green-600 rounded text-lg mt-10 sm:mt-0"
            >
              Upload
            </a>
          </div>
        </div>
      </section>

      <Footer></Footer>
    </div>
  );
}
