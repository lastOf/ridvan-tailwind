import Nav2 from "../components/nav2";
import Hero from "../components/hero";
import Card from "../components/card";
import Footer from "../components/footer";
import PageHead from "../components/head";
import Websites from "../components/websites";
import Example from "../components/example";
import Break from "../components/break";
import Apps from "../components/apps";
export default function IndexPage() {
  return (
    <div>
      <PageHead></PageHead>
      <Nav2></Nav2>

      <Hero></Hero>
      {/* <section className="text-gray-700 body-font">

      <div className="container px-5 py-24 mx-auto">
      <div className="flex flex-wrap mb-20 w-full">
        <div className="mb-6 w-full lg:w-1/2 lg:mb-0">
          <h1 className="mb-2 text-2xl font-medium text-gray-900 sm:text-3xl title-font">
            Pitchfork Kickstarter Taxidermy
          </h1>
          <div className="w-20 h-1 bg-teal-700 rounded"></div>
        </div>
        <p className="w-full text-base leading-relaxed lg:w-1/2">
          Whatever cardigan tote bag tumblr hexagon brooklyn asymmetrical
          gentrify, subway tile poke farm-to-table. Franzen you probably haven't
          heard of them man bun deep jianbing selfies heirloom prism food truck
          ugh squid celiac humblebrag.
        </p>
      </div>
      </div>
      <Card></Card>
      </section> */}
      <Break />
      {/* <section className="text-gray-400 bg-gray-900 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col items-start mx-auto lg:w-2/3 sm:flex-row sm:items-center">
            <h1 className="flex-grow text-2xl font-medium text-white sm:pr-16 title-font">
              Looking to start small?
              <br /> Check out our <i>No Business Too Small</i> program.
            </h1>
            <a href="/small">
              <button className="flex-shrink-0 px-8 py-2 mt-10 text-lg text-white bg-teal-600 rounded border-0 focus:outline-none hover:bg-teal-700 sm:mt-0">
                $50 Websites
              </button>
            </a>
          </div>
        </div>
        <Break fill="#1A202C" />
      </section> */}

      <section id="opensource" className="text-gray-700 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-wrap mb-20 w-full">
            <div className="mb-6 w-full lg:w-1/2 lg:mb-0">
              <h2 className="mb-2 text-2xl font-medium text-gray-900 sm:text-3xl title-font">
                The Best of Open-Source
              </h2>
              <div className="w-20 h-1 bg-teal-700 rounded"></div>
            </div>
            <p className="w-full text-base leading-relaxed lg:w-1/2">
              For just about every situation there is an open source tool
              already built. The vast open source ecosystem is full of options.
              Each project, almost as a living organism, has its own vitals.
              Some are healthy and thriving, others may be on life support.
              Ridván can help you navigate the options and get up and running
              with the best tool for the job.
            </p>
          </div>
          <div className="flex flex-wrap -m-4">
            {[
              {
                title: "Discourse",
                subtitle: "Civilized Discussion",
                paragraph: `"Discourse is a from-scratch reboot, an attempt to reimagine what a modern Internet discussion forum should be today, in a world of ubiquitous smartphones, tablets, Facebook, and Twitter."`,
                image: "/discourse.jpg",
              },
              {
                title: "Ghost",
                subtitle: "Powerful Publishing",
                paragraph: `"Publish on your terms. You control the design, the content, the experience, and everything in between. No broken algorithms or social network rules. You're in charge."`,
                image: "/ghost.jpg",
              },
              {
                title: "OpenCart",
                subtitle: "Complete Ecommerce",
                paragraph: `"From corporations to local businesses. OpenCart powers over 342 000 eCommerce Entrepreneurs all over the world."`,
                image: "/opencart.jpg",
              },
              {
                title: "Zulip",
                subtitle: "Chat for Distributed Teams",
                paragraph: `"Zulip combines the immediacy of real-time chat with an email threading model.
                With Zulip, you can catch up on important conversations while ignoring irrelevant ones."`,
                image: "/Zulip.jpg",
              },
              {
                title: "And More",
                subtitle: "Oh so many more",
                paragraph: `These are just a few examples of the excellent software available. Everything from deploying your own online radio-station to customer relationship management tools.`,
                image: "/more.jpg",
              },
            ].map((el) => {
              return <Card key={el.title} data={el}></Card>;
            })}
            <div className="container px-5 py-12 mx-auto">
              <div className="flex flex-col items-start mx-auto lg:w-2/3 sm:flex-row sm:items-center">
                <h1 className="flex-grow text-2xl font-medium text-gray-900 sm:pr-16 title-font">
                  Don't know whats best for your situation? Lets talk it over in
                  a consultation and we'll find the best tool available.
                </h1>
                <button className="flex-shrink-0 px-8 py-2 mt-10 text-lg text-white bg-teal-700 rounded border-0 focus:outline-none hover:bg-teal-600 sm:mt-0">
                  Reach Out to lastof@pm.me
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Break />

      <Websites></Websites>
      <Break />
      <Apps />
      <Break />
      <section id="recent">
        <div className="px-5 pt-20 mx-auto">
          <div className="mb-20 text-center">
            <h1 className="mb-4 text-2xl font-medium text-gray-900 sm:text-3xl title-font">
              Most Recent work
            </h1>
            <p className="mx-auto text-base leading-relaxed xl:w-2/4 lg:w-3/4"></p>
            <div className="flex justify-center mt-6">
              <div className="inline-flex w-16 h-1 bg-teal-700 rounded-full"></div>
            </div>
          </div>
        </div>
        {[
          {
            title: "Brown and Kirk Mobile Detailing",
            subtitle: "Best mobile detailers in the CSRA.",
            image: "/bk.jpg",
            avatar: "/brown.jpg",
            paragraph:
              "Brown and Kirk Mobile Detailing was founded by Steve Brown in 2019. As the owner and founder Mr. Brown always had a vision to serve those in need of detailing, but also to be distinguished by oustanding work.",
            link: "https://brownandkirk.com/",
          },
          {
            title: "Promise of World Peace",
            subtitle: "A small site for a big event.",
            image: "/pwp.png",
            avatar: "/nighFront.jpg",
            paragraph:
              "For the first time in history it is possible for everyone to view the entire planet, with all its myriad diversified peoples, in one perspective. World peace is not only possible but inevitable.",
            link: "https://www.the-edmond-star.us/",
          },
          {
            title: "ShedSpread",
            subtitle: "Finally get your life in order",
            image: "/shed.png",
            avatar: "/logo.png",
            paragraph:
              "ShedSpread is a tool for organizing your things. Take a picture of your cabinet, fridge, or shelf, you can place boxes on specific parts of an image, label it, and fill it with a list of items. ",
            link: "https://shedspread.now.sh",
          },
          {
            title: "Genesis Enforcement",
            subtitle: "Atlanta based Security agency.",
            image: "/enforcement.jpg",
            avatar: "/vict.jpg",
            paragraph:
              "Genesis Enforcement needed a single static page, with a smooth load time, that communicated their Atlanta roots.",
            link: "https://enforcement.now.sh",
          },
          {
            title: "Discourse at Wilmette",
            subtitle: "Community Discussions for the Wilmette Institute",
            image: "/discatwilm.jpg",
            avatar: "/wilm-196x196.jpg",
            paragraph:
              "Looking for a healthy environment for its upword aiming commmunity, Discourse is the perfect tool for the Wilmette Institute discussions on eliminating racial prejudice.",
            link: "https://discourse.wilmetteinstitute.org",
          },
        ].map((el) => (
          <Example key={el.title} data={el} />
        ))}
      </section>
      <div className="container px-5 py-12 mx-auto">
        <div className="flex flex-col items-start mx-auto lg:w-2/3 sm:flex-row sm:items-center">
          <h1 className="flex-grow text-2xl font-medium text-gray-900 sm:pr-16 title-font">
            Whether you've got a few questions or a brilliant idea!
          </h1>
          <button className="flex-shrink-0 px-8 py-2 mt-10 text-lg text-white bg-teal-700 rounded border-0 focus:outline-none hover:bg-teal-600 sm:mt-0">
            Reach Out to lastof@pm.me
          </button>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
}
