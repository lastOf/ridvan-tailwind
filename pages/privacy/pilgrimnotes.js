import Nav2 from "../../components/nav2";
import Hero from "../../components/hero";
import Card from "../../components/card";
import Footer from "../../components/footer";

import Websites from "../../components/websites";
import Example from "../../components/example";

export default function Privacy() {
  let name = "My Bahá’í Pilgrimage Journal"
  return (
    <div>
      <Nav2></Nav2>
      <section className="overflow-hidden text-gray-700 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col mb-20 w-full text-center">
            <h1 className="mb-2 text-3xl font-medium text-gray-900 sm:text-4xl title-font">
              {name} Privacy Policy
            </h1>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              This privacy policy governs your use of the software application 
               {" "}{name} (“Application”) for mobile devices that was created by
              Ridván. The Application is an organizational utility application,
              aimed at any ages for the purpose of organizing physical items
              with lists and photos.
            </p>
            <h2 className="mx-auto mt-4 text-lg font-medium text-gray-900 title-font">
              What information does the Application obtain and how is it used?
            </h2>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              The Application does not collect any information. All images and
              data input are stored locally on the device and never transferred
              to an external server.
            </p>
            <h2 className="mx-auto mt-4 text-lg font-medium text-gray-900 title-font">
              Does the Application collect precise real time location
              information of the device?
            </h2>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              No location information is collected.
            </p>
            <h2 className="mx-auto mt-4 text-lg font-medium text-gray-900 title-font">
              Do third parties see and/or have access to information obtained by
              the Application?
            </h2>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              No information is obtained. Nothing is shared with third parties.
            </p>
            <h2 className="mx-auto mt-4 text-lg font-medium text-gray-900 title-font">
              Your Consent
            </h2>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              By using the Application, you are not consenting to our processing
              or the collecting of your information.
            </p>
            <h2 className="mx-auto mt-4 text-lg font-medium text-gray-900 title-font">
              Contact us
            </h2>
            <p className="mx-auto text-base leading-relaxed lg:w-2/3">
              If you have any questions regarding privacy while using the
              Application, or have questions about our practices, please contact
              us via email at TheGlobePressNYC@gmail.com. If you have any questions regarding
              privacy while using the Application, or have questions about our
              practices, please contact us via email at TheGlobePressNYC@gmail.com.
            </p>
          </div>
        </div>
      </section>

      <Footer></Footer>
    </div>
  );
}
